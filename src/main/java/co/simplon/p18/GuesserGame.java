package co.simplon.p18;

import java.util.ArrayList;

public class GuesserGame {

    public static int MAX_RANDOM = 50;
    public UserInput uip;
    public int chosenNumber;
    public ArrayList<Integer> played;

    public GuesserGame() {
        this.uip = new UserInput();
        this.played = new ArrayList<Integer>();
    }

    public int getRandomNumber(int max) {
        // https://stackoverflow.com/a/363692
        return java.util.concurrent.ThreadLocalRandom.current().nextInt(1, max + 1);
    }

    public boolean checkResult(int userNumber) {
        boolean result = false;
        if (userNumber == this.chosenNumber) {
            result = true;
            System.out.println("Gagné !");
        } else {
            if (userNumber > this.chosenNumber) {
                System.out.println("Trop grand !");
            } else {
                System.out.println("Trop petit !");
            }
        }
        return result;
    }

    public void displayPlayed() {
        StringBuffer s = new StringBuffer();
        s.append("Coups joués : ");
        if (this.played.size() > 1) { 
            int i;
            for (i=0; i<this.played.size()-1; i++) {
                s.append(this.played.get(i) + ", ");
            }
            s.append( "et " + this.played.get(i));
        }
        if (this.played.size() == 1) {
            s.append(this.played.get(0));
        }
        System.out.println(s.toString());
    }

    public void run() {
        boolean finished = false;
        this.chosenNumber = this.getRandomNumber(GuesserGame.MAX_RANDOM);
        while (!finished) {
            this.displayPlayed();
            int userNumber = this.uip.getUserInput();
            this.played.add(userNumber);
            finished = this.checkResult(userNumber);
        }

    }



}
