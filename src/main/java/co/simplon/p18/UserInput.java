package co.simplon.p18;

import java.util.Scanner;

public class UserInput {
    Scanner s;
    public UserInput() {
        this.s = new Scanner(System.in);
    }

    public int getUserInput() {
        System.out.println("Merci de choisir votre valeur : ");
        return Integer.parseInt(s.nextLine());
    }
}
