# Projet GuesserGame

Le but de ce projet est de coer un jeu simple : le nombre mystère.

La machine choisit un nombre au hasard, l'utilisateur doit le deviner.
À chaque tentative, le programme lui dit "trop haut" si son nombre est plus élevé que le nombre à deviner, "trop bas" si son nombre est plus petit que le nombre à deviner, ou "gagné" s'il a trouvé le bon nombre.

## Algorithme simple

```
machine_number <- La machine choisit un nombre
TANT QUE l'utilisateur n'a pas gagné FAIRE
    user_number <- L'utilisateur choisit un nombre
    SI user_number = machine_number ALORS
        Afficher "Gagné"
    SINON
        SI user_number > machine_number ALORS
            Afficher "Trop grand"
        SINON
            Afficher "Trop petit"
        FIN SI
    FIN SI
FIN TANT QUE

```

## Étapes de développement

1. Mise en place du projet : création de la classe GuesserGame, de la classe UserInput, et du squelette des méthodes
2. Remplissage de la méthode getRandomNumber(int max)
3. Remplissage de la méthode getUserInput()
4. Remplissage de la méthode checkResult()
5. Ajout de la boucle